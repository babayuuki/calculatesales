package jp.alhinc.calculate_sales;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class CalculateSales {

	// 支店定義ファイル名
	private static final String FILE_NAME_BRANCH_LST = "branch.lst";

	// 支店別集計ファイル名
	private static final String FILE_NAME_BRANCH_OUT = "branch.out";

	//商品定義ファイル名
	private static final String FILE_NAME_COMMODITY_LST = "commodity.lst";

	//商品別集計ファイル
	private static final String FILE_NAME_COMMODITY_OUT = "commodity.out";

	// エラーメッセージ
	private static final String UNKNOWN_ERROR = "予期せぬエラーが発生しました";
	private static final String FILE_NOT_EXIST = "定義ファイルが存在しません";
	private static final String FORMAT_ERROR = "定義ファイルのフォーマットが不正です";
	private static final String BRANCH_CODE_ERROR = "の支店コードが不正です";
	private static final String COMMODITY_CODE_ERROR = "の商品コードが不正です";
	private static final String SUM_OVER_ERROR = "合計金額が10桁を超えました";
	private static final String FILE_SKIP_ERROR = "売り上げファイル名が連番になっていません";


	/**
	 * メインメソッド
	 *
	 * @param コマンドライン引数
	 */

	public static void main(String[] args) {

		//コマンドライン引用確認
		if(args.length != 1) {
			System.out.println(UNKNOWN_ERROR);
			return;
		}

		// 支店コードと支店名を保持するMap
		Map<String, String> branchNames = new HashMap<>();

		// 支店コードと売上金額を保持するMap
		Map<String, Long> branchSales = new HashMap<>();

		//商品コードと商品名
		Map<String, String> commodityNames = new HashMap<>();

		//商品コードと売上金額
		Map<String, Long> commoditySales = new HashMap<>();


		// 支店定義ファイル読み込み処理
		if(!readFile(args[0], FILE_NAME_BRANCH_LST, branchNames, branchSales, "^[0-9]{3}$", "支店")) {
			return;
		}

		//商品定義ファイル読み込み
		if(!readFile(args[0], FILE_NAME_COMMODITY_LST, commodityNames, commoditySales, "^[a-zA-Z0-9]{8}$", "商品")) {
			return;
		}

		// ※ここから集計処理を作成してください。(処理内容2-1、2-2)
		File[] files = new File(args[0]).listFiles();
		//ファイル存在チェック
		List<File> rcdFiles = new ArrayList<>();

		for(int i = 0; i < files.length; i++) {
			if(files[i].isFile() && files[i].getName().matches("^[0-9]{8}.rcd$")) {
				rcdFiles.add(files[i]);
			}
		}

		//連番チェック
		Collections.sort(rcdFiles);
		for(int j = 0; j < rcdFiles.size() - 1; j++) {
			int fomer = Integer.parseInt((rcdFiles.get(j).getName()).substring(0, 8));
			int latter = Integer.parseInt((rcdFiles.get(j + 1).getName()).substring(0, 8));

			if((latter - fomer) != 1) {
				System.out.println(FILE_SKIP_ERROR);
				return;
			}
		}

		BufferedReader br = null;

		for(int j = 0; j < rcdFiles.size(); j++) {
			List<String> rcd = new ArrayList<>();
			try {
				File file = rcdFiles.get(j);
				FileReader fr = new FileReader(file);
				br = new BufferedReader(fr);

				String line;

				//一行づつ
				while((line = br.readLine()) != null) {
					rcd.add(line);
				}


				//売り上げファイルフォーマット確認
				if(rcd.size() != 3) {
					System.out.println(rcdFiles.get(j).getName() + FORMAT_ERROR);
					return;
				}

				//key存在確認
				if(!branchNames.containsKey(rcd.get(0))) {
					System.out.println(files[j].getName() + BRANCH_CODE_ERROR);
					return;
				}
				if(!commodityNames.containsKey(rcd.get(1))) {
					System.out.println(files[j].getName() + COMMODITY_CODE_ERROR);
					return;
				}

				//売上金額が数字か
				if(!rcd.get(2).matches("^[0-9]+$")) {
					System.out.println(UNKNOWN_ERROR);
					return;
				}

				//型変換及び加算
				long fileSale = Long.parseLong(rcd.get(2));
				Long saleAmount = branchSales.get(rcd.get(0)) + fileSale;
				Long commoditySaleAmount = commoditySales.get(rcd.get(1)) + fileSale;

				//売上合計10桁超えたか
				if(saleAmount >= 10000000000L || commoditySaleAmount >= 10000000000L) {
					System.out.println(SUM_OVER_ERROR);
					return;
				}

				branchSales.put(rcd.get(0), saleAmount);
				commoditySales.put(rcd.get(1), commoditySaleAmount);

			} catch(IOException e) {
				System.out.println(UNKNOWN_ERROR);
				return;
			} finally {
				// ファイルを開いている場合
				if(br != null) {
					try {
						// ファイルを閉じる
						br.close();
					} catch(IOException e) {
						System.out.println(UNKNOWN_ERROR);
						return;
					}
				}
			}
		}

		if(!writeFile(args[0], FILE_NAME_BRANCH_OUT, branchNames, branchSales)) {
			return;
		}

		if(!writeFile(args[0], FILE_NAME_COMMODITY_OUT, commodityNames, commoditySales)) {
			return;
		}
	}

	/**
	 * 支店定義ファイル読み込み処理
	 * @param
	 *
	 * @param フォルダパス
	 * @param ファイル名
	 * @param 支店コードと支店名を保持するMap
	 * @param 支店コードと売上金額を保持するMap
	 * @return 読み込み可否
	 */
	private static boolean readFile(String path, String fileName, Map<String, String> names, Map<String, Long> sales, String code, String type) {
		BufferedReader br = null;

		try {
			File file = new File(path, fileName);

			//ファイル存在チェック
			if(!file.exists()) {
				System.out.println(type + FILE_NOT_EXIST);
				return false;
			}

			FileReader fr = new FileReader(file);
			br = new BufferedReader(fr);

			String line;
			// 一行ずつ読み込む
			while((line = br.readLine()) != null) {

				// ※ここの読み込み処理を変更してください。(処理内容1-2)ok
				String[] items = line.split(",");

				//支店定義ファイルフォーマット確認
				if((items.length != 2) || (!items[0].matches(code))) {
					System.out.println(type + FORMAT_ERROR);
					return false;
				}

				names.put(items[0], items[1]);
				sales.put(items[0], (long) 0);
			}

		} catch(IOException e) {
			System.out.println(UNKNOWN_ERROR);
			return false;
		} finally {
			// ファイルを開いている場合
			if(br != null) {
				try {
					// ファイルを閉じる
					br.close();
				} catch(IOException e) {
					System.out.println(UNKNOWN_ERROR);
					return false;
				}
			}
		}
		return true;
	}

	/**
	 * 支店別集計ファイル書き込み処理
	 *
	 * @param フォルダパス
	 * @param ファイル名
	 * @param 支店コードと支店名を保持するMap
	 * @param 支店コードと売上金額を保持するMap
	 * @return 書き込み可否
	 */
	private static boolean writeFile(String path, String fileName, Map<String, String> names, Map<String, Long> sales) {
		BufferedWriter bw = null;

		// ※ここに書き込み処理を作成してください。(処理内容3-1)
		try {
			File writeFile = new File(path, fileName);
			FileWriter fw = new FileWriter(writeFile);
			bw = new BufferedWriter(fw);

			for(String key : names.keySet() ) {
				bw.write(key + "," + names.get(key) + "," + sales.get(key));
				bw.newLine();
			}
		} catch(IOException e) {
			System.out.println(UNKNOWN_ERROR);
			return false;
		} finally {
			// ファイルを開いている場合
			if(bw != null) {
				try {
			    	// ファイルを閉じる
					bw.close();
				} catch(IOException e) {
					System.out.println(UNKNOWN_ERROR);
					return false;
				}
			}
		}
		return true;
	}
}
